------------
Mongo JMDict
------------

This is attempt to put the JMDict XML file into mongo.

More of a way to mess around with mongo and angular rather than a serious
effort.

------------
Loading Data
------------

mongoimport jmdict_data.js --db dictionary --collection jmdict

-------
Running
-------

python app.py

