var app = angular.module('EdictSearch', []);

app.controller('SearchCtrl', ['$scope', '$http', function($scope, $http) {

    $scope.entries = [];
    $scope.advanced_search = true;
    $scope.search_term = '';

    $scope.toggleAdvanced = function() {
        $scope.advanced_search = !$scope.advanced_search;
    }

    $scope.clearSearch = function() {
        $scope.entries = [];
    }

    $scope.doSearch = function() {
        var search_term = this.search_term;
        if (!search_term) {
            alert('Please enter a search term');
            this.clearSearch();
            return;
        }

        var langs = [];
        // Couldn't find a good way to do this in angular
        jQuery('[id^=lang]:checked').each(function(r) {
            langs.push($(this).val());
        });
        if (langs.length == 0) {
            langs = ['eng'];
        }

        var params = {
            search_term: search_term,
            lang: langs
        };

        var config = {
            url: 'query',
            method: 'get',
            params: params
        };

        $http.get(config.url, config)
            .success(function(data) {
                $scope.entries  = data.entries;
            })
            .error(function(data, http_status, headers, config) {
                alert('Error: ' + http_status);
            });
    }

}]);
