# TODO - set up so you don't have to pass in the db each time.

def get_languages(db):
    return db.jmdict.distinct('glosses.lang')

def get_parts_of_speech(db):
    return db.jmdict.distinct('glosses.pos')

def search_dictionary(db, search_term, languages=None, parts_of_speech=None):

    if not languages:
        languages = ['eng']

    regex_term = '.*{}.*'.format(search_term)

    lang_query = [{'lang': l} for l in languages]

    query = {
        '$or': [
            {
                'glosses': {
                    '$elemMatch': {
                        '$or': lang_query,
                        'gloss': { '$regex' : regex_term },
                    }
                }
            },
            { 'kanas': { '$regex': regex_term }},
            { 'kanjis': { '$regex': regex_term }},
        ]
    }

    if parts_of_speech and len(parts_of_speech) > 0:
        query['glosses.pos'] = {'$in': parts_of_speech}

    return db.jmdict.find(query)

