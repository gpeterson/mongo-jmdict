import json

import pymongo

from flask import (
    g,
    Flask,
    jsonify,
    render_template,
    request
)

from models import mongo
from models.utils import JmdictJSONEncoder

# TODO - move to a settings file.
MONGO_URI = 'mongodb://localhost:27017/dictionary'

app = Flask(__name__)

@app.before_request
def setup_mongo_client():
    client = pymongo.MongoClient(MONGO_URI)
    g.db = client.dictionary

@app.route('/')
def index():

    languages = mongo.get_languages(g.db)
    parts_of_speech = mongo.get_parts_of_speech(g.db)

    return render_template('jmdict/index.html',
        languages=languages,
        parts_of_speech=parts_of_speech,
    )

@app.route('/query')
def query():

    search_term = request.values.get('search_term', '')
    languages = request.values.getlist('lang')
    parts_of_speech = request.values.getlist('pos')

    entries = mongo.search_dictionary(g.db, search_term, languages, parts_of_speech)

    # todo - make a generator?
    return json.dumps({'entries': list(entries)}, cls=JmdictJSONEncoder)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)

